# -*- coding: utf-8 -*-
import re

from Stack import *
from ItextBuildData import *

class TexstyleValidator:

    def __init__(self):
        self.stack = Stack()

    """
    When there is only closing tag left we need to simulate
    opening tag value because when .span() won't find group it
    will return "None" which cannot be compared with numbers.
    Method is not immune for closing tag None
    """
    def isOpeningTagBeforeClosing(self, openingTag, colsingTag):

        closingTagPosition = colsingTag.span(1)[1]

        if openingTag == None:
            openTagPosition = colsingTag.span(2)[1] + 1
        else:
            openTagPosition = openingTag.span(1)[1]

        if colsingTag == None:
            closingTagPosition = openingTag.span(1)[1] + 1
        else:
            closingTagPosition = colsingTag.span(2)[1]

        if openTagPosition < closingTagPosition:
            return True
        else:
            return False

    def noTagsLeft(self, openingTag, closingTag):
        if openingTag == None and closingTag == None:
            return True
        else:
            return False

    def validate(self, ex):

        openingTag = "^(.*?)(<<style:.*?>>)"
        closingTag = "^(.*?)(<<\/style>>)"

        while (len(ex) > 0):

            openingMatch = re.match(openingTag, ex)
            closingMatch = re.match(closingTag, ex)

            if self.noTagsLeft(closingMatch, closingMatch): break

            if self.isOpeningTagBeforeClosing(openingMatch, closingMatch):
                self.stack.push(1)
                ex = ex[openingMatch.span(2)[1]:]
            else:
                if self.stack.isEmpty():
                    print("Validation error!")
                    break
                self.stack.pull()
                ex = ex[closingMatch.span(2)[1]:]

        if not self.stack.isEmpty():
            print("Validation error!")
        else:
            print("Code good")





