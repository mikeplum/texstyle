# -*- coding: utf-8 -*-
import re

from Stack import *
from ItextBuildData import *
from TexstyleValidator import *

class TexstyleGenerator:

    def __init__(self):
        self.validator = TexstyleValidator()
        self.tagStack = Stack()
        self.results = []

    def setResults(self, results):
        self.results = results

    def getResults(self):
        return self.results

    def printResults(self):
        if self.getResults() == []:
            print ("There is no results!")
        else:
            for i in sorted(self.getResults(), key=lambda result: result[2]):
                print(i.getAsDict())

    """
    Method for protection from no "getStyle()" element error 
    """
    def getStyle(self, stack):
        if stack.peek() is None or stack.isEmpty():
            styleToAdd = ""
        else:
            styleToAdd = self.tagStack.peek().getStyle()
        return styleToAdd

    """
    Main method which are resposible for preparing data collection for further 
    preparation in composeing ITEXT data.
    """
    def prepare(self, ex):

        textOrder = 0
        itextBuildData = []

        anyTag = "^(.*?)<<[\/]?style(.*?)>>"
        openingTag = "<<style:(.*?)>>(.*?)<<"
        closingTag = "^(.*?)(<<\/style>>)"
        textBeforeTag = "^(.*?)<<style:"

        allStyleMarkupQuantity = re.findall("<<style:", ex)
        allStyleMarkupQuantity = len(allStyleMarkupQuantity)

        if allStyleMarkupQuantity > 0:

            while (len(ex) > 0):

                #Handle No-Tag situation
                #If there is no <<style>> tag - break the loop and add what has left in string to result.
                if re.match(anyTag, ex) == None:
                    itextBuildData.append(ItextBuildData(textOrder, "", ex))
                    break

                openingMatch = re.search(openingTag, ex)
                closingMatch = re.search(closingTag, ex)

                #Handle opening tags <<style:some_style>>
                if self.validator.isOpeningTagBeforeClosing(openingMatch, closingMatch):

                    #Handle text which is before opening tag or in the middle passed by other tag
                    #"textToHandle_noStyle<<style:1>>styl_1<<style:2>>styl_2<</style>>textTuHandle_style_1<<style:4>>styl_4<</style>>bez_stylu"
                    textBeforeOpeningTag = re.match(textBeforeTag, ex)
                    if (textBeforeOpeningTag is not None and textBeforeOpeningTag.group(1) != ''):
                        itextBuildData.append(ItextBuildData(textOrder, self.getStyle(self.tagStack), textBeforeOpeningTag.group(1)))
                        ex = ex[textBeforeOpeningTag.span(1)[1]:]
                        textOrder += 1

                    # Handle text beetween opening tag
                    openingNodeWithValue = re.search(openingTag, ex)
                    self.tagStack.push(ItextBuildData(textOrder, openingNodeWithValue.group(1), openingNodeWithValue.group(2)))
                    ex = ex[openingNodeWithValue.span(2)[1]:]
                    textOrder += 1

                #if there is no closing tag go to next iteration
                if self.validator.isOpeningTagBeforeClosing(openingMatch, closingMatch): continue

                #Handle closeing tags <<style:some_style>>
                if not self.tagStack.isEmpty():

                    #Handle text before closing tags if some text was left i.e.  "dssds<</style>>"
                    #and some opening tag should attach to it some style
                    if closingMatch.group(1) != "" :
                        itextBuildData.append(ItextBuildData(textOrder, self.tagStack.peek().getStyle(), closingMatch.group(1)))
                        textOrder += 1

                    ex = ex[closingMatch.span(2)[1]:]
                    itextBuildData.append(self.tagStack.pull())

        self.setResults(itextBuildData)