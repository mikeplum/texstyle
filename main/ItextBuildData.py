class ItextBuildData:
    def __init__(self, elementOrder, style, text):
        self.elementOrder = elementOrder
        self.style = style
        self.text = text

    def getOrder(self):
        return self.elementOrder

    def getStyle(self):
        return self.style

    def getText(self):
        return self.text

    def getAsDict(self):
        return { "order" : self.getOrder() , "style" : self.getStyle(), "text" : self.getText() }

    def __getitem__(self, item):
        return self.getOrder()
