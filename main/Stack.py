class Stack:
    def __init__(self):
        # type: () -> object
        self.items = []

    def isEmpty(self):
        return self.items == []

    def push(self, item):
        self.items.append(item)

    def pull(self):
        return self.items.pop()

    def peek(self):
        if len(self.items) == 0:
            return self.items
        else:
            return self.items[len(self.items) - 1]

    def size(self):
        return len(self.items)
