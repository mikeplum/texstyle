# -*- coding: utf-8 -*-
from Texstyle import *

texstyle = TexstyleGenerator()

itWorks = "dss<</style<<style:styl_1>>Ala<<style:styl_2>>ma<</style>>ds<</style>>środek<<style:styl_3>>Ala<</style>>dsds"

bug1 = "321321<style:1>>styl_1<<style:2>>styl_2<<style:3>>styl_3<</style>>styl_2<</style>>styl_1<<style:4>>styl_4<</style>>styl_1<</style>>bez_stylu"

bug2 = "textToHandle_noStyle<<style:1>>styl_1<<style:2>>styl_2<</style>>textTuHandle_style_1<<style:4>>styl_4<</style>>bez_stylu"

bug3 = "<<style:1>>styl_1<<style:2>>styl_2<<style:3>><</style>>styl_3styl_2<</style>>styl_1<<style:4>>styl_4<</style>>styl_1<</style>>bez_stylu"

bug4 = "dss<<style:1>>styl_1<</style>><<style:2>>styl_2<</style>><</style>>"

texstyle2 = TexstyleValidator()
texstyle2.validate(itWorks)

texstyle.prepare(itWorks)
texstyle.printResults()

